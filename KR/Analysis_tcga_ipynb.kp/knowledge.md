---
authors:
- nikhil.shenoy@elucidata.io
created_at: 2019-05-27 00:00:00
tags:
- Analysis
- Cross-Matching Cancer
thumbnail: images/output_25_0.png
title: Analysis of TCGA Breast Cancer Dataset
tldr: Done PCA on TCGA data of Breast Cancer to verify 5 subtypes
updated_at: 2019-06-12 11:15:41.268703
---
```python
# importing modules
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import seaborn as sns
```
## TCGA Dataset

#### Reading the gene expression file and sample features file


```python
# Meta_data has both tumor and normal samples
gene_exp_df = pd.read_csv('./src/HiSeqV2BRCA', sep = "\t")
meta_data = pd.read_csv('./src/BRCA_clinicalMatrix.csv',sep = "\t", index_col= [0])
```

```python
print ("Dimensions of gene_exp_df : " + str(gene_exp_df.shape))
print ("Dimensions of meta_data : " + str(meta_data.shape))
```
    Dimensions of gene_exp_df : (20530, 1219)
    Dimensions of meta_data : (1247, 202)



```python
# Rows are genes and columns are samples
gene_exp_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>sample</th>
      <th>TCGA-AR-A5QQ-01</th>
      <th>TCGA-D8-A1JA-01</th>
      <th>TCGA-BH-A0BQ-01</th>
      <th>TCGA-BH-A0BT-01</th>
      <th>TCGA-A8-A06X-01</th>
      <th>TCGA-A8-A096-01</th>
      <th>TCGA-BH-A0C7-01</th>
      <th>TCGA-AC-A5XU-01</th>
      <th>TCGA-PE-A5DE-01</th>
      <th>...</th>
      <th>TCGA-A7-A13E-11</th>
      <th>TCGA-C8-A8HP-01</th>
      <th>TCGA-E9-A5FL-01</th>
      <th>TCGA-AC-A2FB-11</th>
      <th>TCGA-E2-A15F-01</th>
      <th>TCGA-A2-A3XT-01</th>
      <th>TCGA-B6-A0X7-01</th>
      <th>TCGA-BH-A1EV-11</th>
      <th>TCGA-3C-AALJ-01</th>
      <th>TCGA-B6-A0X1-01</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>ARHGEF10L</td>
      <td>9.5074</td>
      <td>7.4346</td>
      <td>9.3216</td>
      <td>9.0198</td>
      <td>9.6417</td>
      <td>9.7665</td>
      <td>10.0931</td>
      <td>9.1524</td>
      <td>9.9398</td>
      <td>...</td>
      <td>9.6265</td>
      <td>10.1826</td>
      <td>9.9199</td>
      <td>9.9090</td>
      <td>10.0334</td>
      <td>11.5144</td>
      <td>10.5745</td>
      <td>9.4048</td>
      <td>10.9468</td>
      <td>10.3164</td>
    </tr>
    <tr>
      <th>1</th>
      <td>HIF3A</td>
      <td>1.5787</td>
      <td>3.6607</td>
      <td>2.7224</td>
      <td>1.3414</td>
      <td>0.5819</td>
      <td>0.2738</td>
      <td>3.6090</td>
      <td>0.4738</td>
      <td>2.9378</td>
      <td>...</td>
      <td>8.1546</td>
      <td>2.2159</td>
      <td>3.8645</td>
      <td>8.1872</td>
      <td>0.8836</td>
      <td>1.3169</td>
      <td>4.0696</td>
      <td>7.2537</td>
      <td>0.9310</td>
      <td>2.4191</td>
    </tr>
    <tr>
      <th>2</th>
      <td>RNF17</td>
      <td>0.0000</td>
      <td>0.6245</td>
      <td>0.5526</td>
      <td>0.0000</td>
      <td>0.0000</td>
      <td>0.8765</td>
      <td>0.0000</td>
      <td>0.0000</td>
      <td>0.0000</td>
      <td>...</td>
      <td>0.0000</td>
      <td>0.0000</td>
      <td>3.7305</td>
      <td>0.0000</td>
      <td>0.0000</td>
      <td>1.1329</td>
      <td>0.4258</td>
      <td>0.0000</td>
      <td>0.0000</td>
      <td>0.0000</td>
    </tr>
    <tr>
      <th>3</th>
      <td>RNF10</td>
      <td>11.3676</td>
      <td>11.9181</td>
      <td>11.9665</td>
      <td>13.1881</td>
      <td>12.0036</td>
      <td>11.8118</td>
      <td>11.3820</td>
      <td>11.5004</td>
      <td>12.2055</td>
      <td>...</td>
      <td>11.9869</td>
      <td>12.2653</td>
      <td>12.4815</td>
      <td>11.8263</td>
      <td>12.0135</td>
      <td>11.5818</td>
      <td>11.8663</td>
      <td>11.5460</td>
      <td>12.2616</td>
      <td>12.1570</td>
    </tr>
    <tr>
      <th>4</th>
      <td>RNF11</td>
      <td>11.1292</td>
      <td>13.5273</td>
      <td>11.4105</td>
      <td>11.0911</td>
      <td>11.2545</td>
      <td>10.8554</td>
      <td>10.7663</td>
      <td>10.4358</td>
      <td>11.2210</td>
      <td>...</td>
      <td>11.9344</td>
      <td>11.4117</td>
      <td>10.4902</td>
      <td>11.5754</td>
      <td>10.8370</td>
      <td>10.2736</td>
      <td>10.7644</td>
      <td>11.8774</td>
      <td>10.7842</td>
      <td>11.2420</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 1219 columns</p>
</div>




```python
meta_data.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>AJCC_Stage_nature2012</th>
      <th>Age_at_Initial_Pathologic_Diagnosis_nature2012</th>
      <th>CN_Clusters_nature2012</th>
      <th>Converted_Stage_nature2012</th>
      <th>Days_to_Date_of_Last_Contact_nature2012</th>
      <th>Days_to_date_of_Death_nature2012</th>
      <th>ER_Status_nature2012</th>
      <th>Gender_nature2012</th>
      <th>HER2_Final_Status_nature2012</th>
      <th>Integrated_Clusters_no_exp__nature2012</th>
      <th>...</th>
      <th>_GENOMIC_ID_TCGA_BRCA_mutation_wustl_gene</th>
      <th>_GENOMIC_ID_TCGA_BRCA_miRNA_GA</th>
      <th>_GENOMIC_ID_TCGA_BRCA_exp_HiSeqV2_percentile</th>
      <th>_GENOMIC_ID_data/public/TCGA/BRCA/miRNA_GA_gene</th>
      <th>_GENOMIC_ID_TCGA_BRCA_gistic2thd</th>
      <th>_GENOMIC_ID_data/public/TCGA/BRCA/miRNA_HiSeq_gene</th>
      <th>_GENOMIC_ID_TCGA_BRCA_G4502A_07_3</th>
      <th>_GENOMIC_ID_TCGA_BRCA_exp_HiSeqV2</th>
      <th>_GENOMIC_ID_TCGA_BRCA_gistic2</th>
      <th>_GENOMIC_ID_TCGA_BRCA_PDMarray</th>
    </tr>
    <tr>
      <th>sampleID</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>TCGA-3C-AAAU-01</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>6ef883fc-81f3-4089-95e0-86904ffc0d38</td>
      <td>NaN</td>
      <td>TCGA-3C-AAAU-01A-11D-A41E-01</td>
      <td>TCGA-3C-AAAU-01</td>
      <td>NaN</td>
      <td>6ef883fc-81f3-4089-95e0-86904ffc0d38</td>
      <td>TCGA-3C-AAAU-01A-11D-A41E-01</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>TCGA-3C-AALI-01</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>dd8d3665-ec9d-45be-b7b9-a85dac3585e2</td>
      <td>NaN</td>
      <td>TCGA-3C-AALI-01A-11D-A41E-01</td>
      <td>TCGA-3C-AALI-01</td>
      <td>NaN</td>
      <td>dd8d3665-ec9d-45be-b7b9-a85dac3585e2</td>
      <td>TCGA-3C-AALI-01A-11D-A41E-01</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>TCGA-3C-AALJ-01</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>c924c2a8-ab41-4499-bb30-79705cc17d45</td>
      <td>NaN</td>
      <td>TCGA-3C-AALJ-01A-31D-A41E-01</td>
      <td>TCGA-3C-AALJ-01</td>
      <td>NaN</td>
      <td>c924c2a8-ab41-4499-bb30-79705cc17d45</td>
      <td>TCGA-3C-AALJ-01A-31D-A41E-01</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>TCGA-3C-AALK-01</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>1a19e068-d213-41ef-aebb-104017a883cc</td>
      <td>NaN</td>
      <td>TCGA-3C-AALK-01A-11D-A41E-01</td>
      <td>TCGA-3C-AALK-01</td>
      <td>NaN</td>
      <td>1a19e068-d213-41ef-aebb-104017a883cc</td>
      <td>TCGA-3C-AALK-01A-11D-A41E-01</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>TCGA-4H-AAAK-01</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>...</td>
      <td>NaN</td>
      <td>NaN</td>
      <td>2ea9e472-a408-4ae0-975d-50a566f22b2a</td>
      <td>NaN</td>
      <td>TCGA-4H-AAAK-01A-12D-A41E-01</td>
      <td>TCGA-4H-AAAK-01</td>
      <td>NaN</td>
      <td>2ea9e472-a408-4ae0-975d-50a566f22b2a</td>
      <td>TCGA-4H-AAAK-01A-12D-A41E-01</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 202 columns</p>
</div>




```python
# Some minor changes as alias of 2 genes is present in PAM50 gene set
# ('NUF2' to 'CDCA1') ('NDC80' to 'KNTC2')
gene_exp_df['sample'] = gene_exp_df['sample'].str.replace('NUF2','CDCA1')
gene_exp_df['sample'] = gene_exp_df['sample'].str.replace('NDC80','KNTC2')
```
#### Filtering out the genes present in the PAM50 gene dataset


```python
# PAM50 gene list useful in subtyping breast cancer subtypes
PAM50_gene_df = pd.read_csv('./src/PAM50/bioclassifier_R/pam50_centroids.txt', sep = '\t')
PAM50_gene_list = PAM50_gene_df.iloc[:,0].tolist()
print (PAM50_gene_list)
```
    ['ACTR3B', 'ANLN', 'BAG1', 'BCL2', 'BIRC5', 'BLVRA', 'CCNB1', 'CCNE1', 'CDC20', 'CDC6', 'CDCA1', 'CDH3', 'CENPF', 'CEP55', 'CXXC5', 'EGFR', 'ERBB2', 'ESR1', 'EXO1', 'FGFR4', 'FOXA1', 'FOXC1', 'GPR160', 'GRB7', 'KIF2C', 'KNTC2', 'KRT14', 'KRT17', 'KRT5', 'MAPT', 'MDM2', 'MELK', 'MIA', 'MKI67', 'MLPH', 'MMP11', 'MYBL2', 'MYC', 'NAT1', 'ORC6L', 'PGR', 'PHGDH', 'PTTG1', 'RRM2', 'SFRP1', 'SLC39A6', 'TMEM45B', 'TYMS', 'UBE2C', 'UBE2T']


#### Removing all the genes in gene_expression_df that are not present in the PAM50 gene list


```python
gene_exp_df_mod = gene_exp_df[gene_exp_df['sample'].isin(PAM50_gene_list)]
print (gene_exp_df_mod.shape)
```
    (50, 1219)



```python
gene_exp_df_mod.to_csv('./Generated Files/gene_exp_df_mod.csv')
gene_exp_df_mod.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>sample</th>
      <th>TCGA-AR-A5QQ-01</th>
      <th>TCGA-D8-A1JA-01</th>
      <th>TCGA-BH-A0BQ-01</th>
      <th>TCGA-BH-A0BT-01</th>
      <th>TCGA-A8-A06X-01</th>
      <th>TCGA-A8-A096-01</th>
      <th>TCGA-BH-A0C7-01</th>
      <th>TCGA-AC-A5XU-01</th>
      <th>TCGA-PE-A5DE-01</th>
      <th>...</th>
      <th>TCGA-A7-A13E-11</th>
      <th>TCGA-C8-A8HP-01</th>
      <th>TCGA-E9-A5FL-01</th>
      <th>TCGA-AC-A2FB-11</th>
      <th>TCGA-E2-A15F-01</th>
      <th>TCGA-A2-A3XT-01</th>
      <th>TCGA-B6-A0X7-01</th>
      <th>TCGA-BH-A1EV-11</th>
      <th>TCGA-3C-AALJ-01</th>
      <th>TCGA-B6-A0X1-01</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>811</th>
      <td>KNTC2</td>
      <td>8.8855</td>
      <td>7.1062</td>
      <td>6.8201</td>
      <td>6.7393</td>
      <td>7.4086</td>
      <td>8.0439</td>
      <td>8.6601</td>
      <td>7.6555</td>
      <td>7.3260</td>
      <td>...</td>
      <td>4.3926</td>
      <td>8.5307</td>
      <td>8.7151</td>
      <td>5.1283</td>
      <td>7.7721</td>
      <td>8.8586</td>
      <td>6.4794</td>
      <td>6.6572</td>
      <td>8.9222</td>
      <td>9.1810</td>
    </tr>
    <tr>
      <th>1956</th>
      <td>ERBB2</td>
      <td>10.9073</td>
      <td>15.8848</td>
      <td>13.4812</td>
      <td>13.9334</td>
      <td>12.9645</td>
      <td>12.5698</td>
      <td>13.3987</td>
      <td>12.3627</td>
      <td>11.9588</td>
      <td>...</td>
      <td>11.4161</td>
      <td>17.5226</td>
      <td>11.8304</td>
      <td>12.0185</td>
      <td>12.7096</td>
      <td>10.3956</td>
      <td>13.2577</td>
      <td>13.6596</td>
      <td>13.4346</td>
      <td>12.4393</td>
    </tr>
    <tr>
      <th>2674</th>
      <td>TMEM45B</td>
      <td>6.0888</td>
      <td>12.4089</td>
      <td>8.0906</td>
      <td>7.6768</td>
      <td>0.5819</td>
      <td>9.5112</td>
      <td>4.1002</td>
      <td>6.9112</td>
      <td>7.6014</td>
      <td>...</td>
      <td>6.0841</td>
      <td>9.0305</td>
      <td>3.3282</td>
      <td>6.4228</td>
      <td>2.4977</td>
      <td>1.3169</td>
      <td>7.7288</td>
      <td>7.7190</td>
      <td>2.6870</td>
      <td>3.2775</td>
    </tr>
    <tr>
      <th>3530</th>
      <td>KRT14</td>
      <td>18.7069</td>
      <td>0.0000</td>
      <td>11.9467</td>
      <td>10.5771</td>
      <td>0.9953</td>
      <td>1.0320</td>
      <td>9.1678</td>
      <td>9.5712</td>
      <td>10.2550</td>
      <td>...</td>
      <td>11.6481</td>
      <td>6.0314</td>
      <td>11.1995</td>
      <td>15.1339</td>
      <td>10.1576</td>
      <td>9.8426</td>
      <td>10.2115</td>
      <td>13.1876</td>
      <td>1.8953</td>
      <td>10.1757</td>
    </tr>
    <tr>
      <th>3580</th>
      <td>BCL2</td>
      <td>7.7186</td>
      <td>6.5194</td>
      <td>11.1731</td>
      <td>7.6104</td>
      <td>9.0200</td>
      <td>11.4754</td>
      <td>11.0784</td>
      <td>12.4553</td>
      <td>10.1879</td>
      <td>...</td>
      <td>11.1087</td>
      <td>7.6284</td>
      <td>6.5450</td>
      <td>11.1494</td>
      <td>11.5540</td>
      <td>7.4053</td>
      <td>11.8249</td>
      <td>11.4270</td>
      <td>11.3103</td>
      <td>9.0316</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 1219 columns</p>
</div>



#### Some changes 
1. Meta_data_1 has both tumor and normal samples
2. Meta_data_2 has only tumor samples
3. Replace the word normal in tumor samples by normal-like similarly in both tumor_meta_1 and tumor_meta_2


```python
meta_data['sample_type'].unique()

# Getting tumor_meta from meta_data
tumor_meta = pd.DataFrame(meta_data[meta_data['sample_type'] == 'Primary Tumor']['PAM50Call_RNAseq'])
# In tumor samples replacing Normal with Normal-like
tumor_meta["PAM50Call_RNAseq"]= tumor_meta["PAM50Call_RNAseq"].replace('Normal', 'Normal-like') 

# Getting normal_meta from meta_data
normal_meta = pd.DataFrame(meta_data[meta_data['sample_type'] == 'Solid Tissue Normal']['PAM50Call_RNAseq'])
```

```python
print (normal_meta['PAM50Call_RNAseq'].unique())
print (tumor_meta['PAM50Call_RNAseq'].unique())
```
    [nan 'Normal' 'LumA']
    [nan 'Normal-like' 'LumA' 'LumB' 'Basal' 'Her2']



```python
tumor_meta.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PAM50Call_RNAseq</th>
    </tr>
    <tr>
      <th>sampleID</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>TCGA-3C-AAAU-01</th>
      <td>NaN</td>
    </tr>
    <tr>
      <th>TCGA-3C-AALI-01</th>
      <td>NaN</td>
    </tr>
    <tr>
      <th>TCGA-3C-AALJ-01</th>
      <td>NaN</td>
    </tr>
    <tr>
      <th>TCGA-3C-AALK-01</th>
      <td>NaN</td>
    </tr>
    <tr>
      <th>TCGA-4H-AAAK-01</th>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>




```python
normal_meta.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PAM50Call_RNAseq</th>
    </tr>
    <tr>
      <th>sampleID</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>TCGA-A7-A0CE-11</th>
      <td>NaN</td>
    </tr>
    <tr>
      <th>TCGA-A7-A0CH-11</th>
      <td>NaN</td>
    </tr>
    <tr>
      <th>TCGA-A7-A0D9-11</th>
      <td>Normal</td>
    </tr>
    <tr>
      <th>TCGA-A7-A0DB-11</th>
      <td>Normal</td>
    </tr>
    <tr>
      <th>TCGA-A7-A0DC-11</th>
      <td>Normal</td>
    </tr>
  </tbody>
</table>
</div>



#### Preparing data for PCA
gen_exp_T is the transposed data and we want to create a new column for brca_subtypes


```python
# Some formatting to match our samples with metadata
gene_exp_T = gene_exp_df_mod.set_index('sample').T
gene_exp_T['brca_subtype'] = np.nan
gene_exp_T.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th>sample</th>
      <th>KNTC2</th>
      <th>ERBB2</th>
      <th>TMEM45B</th>
      <th>KRT14</th>
      <th>BCL2</th>
      <th>KRT17</th>
      <th>CDC6</th>
      <th>NAT1</th>
      <th>CCNE1</th>
      <th>MLPH</th>
      <th>...</th>
      <th>MYC</th>
      <th>FOXA1</th>
      <th>CXXC5</th>
      <th>EXO1</th>
      <th>EGFR</th>
      <th>CDH3</th>
      <th>CENPF</th>
      <th>MELK</th>
      <th>KIF2C</th>
      <th>brca_subtype</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>TCGA-AR-A5QQ-01</th>
      <td>8.8855</td>
      <td>10.9073</td>
      <td>6.0888</td>
      <td>18.7069</td>
      <td>7.7186</td>
      <td>15.8663</td>
      <td>10.2674</td>
      <td>6.2196</td>
      <td>8.7203</td>
      <td>9.4825</td>
      <td>...</td>
      <td>11.5091</td>
      <td>6.4746</td>
      <td>8.4925</td>
      <td>9.1251</td>
      <td>9.3294</td>
      <td>13.9286</td>
      <td>10.9818</td>
      <td>10.3120</td>
      <td>10.8436</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>TCGA-D8-A1JA-01</th>
      <td>7.1062</td>
      <td>15.8848</td>
      <td>12.4089</td>
      <td>0.0000</td>
      <td>6.5194</td>
      <td>6.8728</td>
      <td>8.5447</td>
      <td>11.3132</td>
      <td>6.9601</td>
      <td>10.6186</td>
      <td>...</td>
      <td>7.0949</td>
      <td>13.3180</td>
      <td>8.0621</td>
      <td>7.7697</td>
      <td>11.3976</td>
      <td>12.3639</td>
      <td>10.3634</td>
      <td>7.6503</td>
      <td>8.5696</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>TCGA-BH-A0BQ-01</th>
      <td>6.8201</td>
      <td>13.4812</td>
      <td>8.0906</td>
      <td>11.9467</td>
      <td>11.1731</td>
      <td>12.0465</td>
      <td>7.3062</td>
      <td>12.4440</td>
      <td>5.4572</td>
      <td>12.2889</td>
      <td>...</td>
      <td>11.4896</td>
      <td>12.5822</td>
      <td>9.1088</td>
      <td>6.5809</td>
      <td>7.9642</td>
      <td>10.4527</td>
      <td>10.0259</td>
      <td>6.9992</td>
      <td>7.6591</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>TCGA-BH-A0BT-01</th>
      <td>6.7393</td>
      <td>13.9334</td>
      <td>7.6768</td>
      <td>10.5771</td>
      <td>7.6104</td>
      <td>11.1618</td>
      <td>7.8233</td>
      <td>7.1234</td>
      <td>5.0536</td>
      <td>12.6656</td>
      <td>...</td>
      <td>10.0480</td>
      <td>13.5317</td>
      <td>8.8176</td>
      <td>7.0791</td>
      <td>6.5980</td>
      <td>10.1759</td>
      <td>9.7163</td>
      <td>7.5761</td>
      <td>7.7168</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>TCGA-A8-A06X-01</th>
      <td>7.4086</td>
      <td>12.9645</td>
      <td>0.5819</td>
      <td>0.9953</td>
      <td>9.0200</td>
      <td>1.8007</td>
      <td>8.9155</td>
      <td>10.3597</td>
      <td>7.2050</td>
      <td>11.6230</td>
      <td>...</td>
      <td>11.0998</td>
      <td>12.4692</td>
      <td>9.4975</td>
      <td>7.8299</td>
      <td>4.2006</td>
      <td>3.7987</td>
      <td>11.8391</td>
      <td>9.5308</td>
      <td>9.4835</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 51 columns</p>
</div>




```python
gene_exp_T['brca_subtype'] = gene_exp_T['brca_subtype'].combine_first(tumor_meta['PAM50Call_RNAseq'])
print (str(gene_exp_T['brca_subtype'].isna().sum()) + " : samples that are nan out of " +
       str(len(gene_exp_T)))

gene_exp_T['brca_subtype'] = gene_exp_T['brca_subtype'].combine_first(normal_meta['PAM50Call_RNAseq'])
print (str(gene_exp_T['brca_subtype'].isna().sum()) + " : samples that are nan out of " +
       str(len(gene_exp_T)))
```
    374 : samples that are nan out of 1218
    267 : samples that are nan out of 1218



```python
gene_exp_T.to_csv('./Generated Files/gene_exp_T.csv', index=False)
gene_exp_T.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th>sample</th>
      <th>KNTC2</th>
      <th>ERBB2</th>
      <th>TMEM45B</th>
      <th>KRT14</th>
      <th>BCL2</th>
      <th>KRT17</th>
      <th>CDC6</th>
      <th>NAT1</th>
      <th>CCNE1</th>
      <th>MLPH</th>
      <th>...</th>
      <th>MYC</th>
      <th>FOXA1</th>
      <th>CXXC5</th>
      <th>EXO1</th>
      <th>EGFR</th>
      <th>CDH3</th>
      <th>CENPF</th>
      <th>MELK</th>
      <th>KIF2C</th>
      <th>brca_subtype</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>TCGA-AR-A5QQ-01</th>
      <td>8.8855</td>
      <td>10.9073</td>
      <td>6.0888</td>
      <td>18.7069</td>
      <td>7.7186</td>
      <td>15.8663</td>
      <td>10.2674</td>
      <td>6.2196</td>
      <td>8.7203</td>
      <td>9.4825</td>
      <td>...</td>
      <td>11.5091</td>
      <td>6.4746</td>
      <td>8.4925</td>
      <td>9.1251</td>
      <td>9.3294</td>
      <td>13.9286</td>
      <td>10.9818</td>
      <td>10.3120</td>
      <td>10.8436</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>TCGA-D8-A1JA-01</th>
      <td>7.1062</td>
      <td>15.8848</td>
      <td>12.4089</td>
      <td>0.0000</td>
      <td>6.5194</td>
      <td>6.8728</td>
      <td>8.5447</td>
      <td>11.3132</td>
      <td>6.9601</td>
      <td>10.6186</td>
      <td>...</td>
      <td>7.0949</td>
      <td>13.3180</td>
      <td>8.0621</td>
      <td>7.7697</td>
      <td>11.3976</td>
      <td>12.3639</td>
      <td>10.3634</td>
      <td>7.6503</td>
      <td>8.5696</td>
      <td>Her2</td>
    </tr>
    <tr>
      <th>TCGA-BH-A0BQ-01</th>
      <td>6.8201</td>
      <td>13.4812</td>
      <td>8.0906</td>
      <td>11.9467</td>
      <td>11.1731</td>
      <td>12.0465</td>
      <td>7.3062</td>
      <td>12.4440</td>
      <td>5.4572</td>
      <td>12.2889</td>
      <td>...</td>
      <td>11.4896</td>
      <td>12.5822</td>
      <td>9.1088</td>
      <td>6.5809</td>
      <td>7.9642</td>
      <td>10.4527</td>
      <td>10.0259</td>
      <td>6.9992</td>
      <td>7.6591</td>
      <td>LumA</td>
    </tr>
    <tr>
      <th>TCGA-BH-A0BT-01</th>
      <td>6.7393</td>
      <td>13.9334</td>
      <td>7.6768</td>
      <td>10.5771</td>
      <td>7.6104</td>
      <td>11.1618</td>
      <td>7.8233</td>
      <td>7.1234</td>
      <td>5.0536</td>
      <td>12.6656</td>
      <td>...</td>
      <td>10.0480</td>
      <td>13.5317</td>
      <td>8.8176</td>
      <td>7.0791</td>
      <td>6.5980</td>
      <td>10.1759</td>
      <td>9.7163</td>
      <td>7.5761</td>
      <td>7.7168</td>
      <td>LumA</td>
    </tr>
    <tr>
      <th>TCGA-A8-A06X-01</th>
      <td>7.4086</td>
      <td>12.9645</td>
      <td>0.5819</td>
      <td>0.9953</td>
      <td>9.0200</td>
      <td>1.8007</td>
      <td>8.9155</td>
      <td>10.3597</td>
      <td>7.2050</td>
      <td>11.6230</td>
      <td>...</td>
      <td>11.0998</td>
      <td>12.4692</td>
      <td>9.4975</td>
      <td>7.8299</td>
      <td>4.2006</td>
      <td>3.7987</td>
      <td>11.8391</td>
      <td>9.5308</td>
      <td>9.4835</td>
      <td>LumB</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 51 columns</p>
</div>



### Running PCA

#### 1. With NaN samples to check if the clustering happens according to brca breast cancer types


```python
# Get Features by taking transpose
features = gene_exp_T
sampleId_to_brca_subtype_map = dict(zip(features.index.tolist(), features['brca_subtype']))
features = features.drop(columns=['brca_subtype'])

# Colors
color_map = {'Basal': 'red',
             np.nan : 'magenta',
 'Her2': 'green',
 'LumA': 'blue',
 'LumB': 'orange',
 'Normal-like': 'brown',
    'Normal' : 'black'}

# Apply PCA
pca = PCA(n_components = 2)
pca.fit(features)
df_pca = pd.DataFrame(pca.transform(features), columns=['PC1', 'PC2'], index=features.index)
plt.scatter(df_pca['PC1'], df_pca['PC2'],
            color=[color_map[sampleId_to_brca_subtype_map[i]] for i in df_pca.index.tolist()])

# Add the axis labels
plt.xlabel('PC 1 (%.2f%%)' % (pca.explained_variance_ratio_[0]*100))
plt.ylabel('PC 2 (%.2f%%)' % (pca.explained_variance_ratio_[1]*100)) 

handlelist = [plt.plot([], marker="o", ls="", color=color)[0] for color in color_map.values()]
plt.legend(handlelist,color_map.keys(),bbox_to_anchor=(1,1), loc="upper left")
plt.savefig('./Plots/PCAwithNaN.png')

plt.show()
```


![png](images/output_25_0.png)


#### 2. Without NaN samples to check if the clustering happens according to brca breast cancer types


```python
# remove tumor having NaN type of breast cancer 
features = gene_exp_T
features = features.dropna(axis=0, subset=['brca_subtype'])
sampleId_to_brca_subtype_map = dict(zip(features.index.tolist(), features['brca_subtype']))

# Colors
color_map = {'Basal': 'red',
 'Her2': 'green',
 'LumA': 'blue',
 'LumB': 'orange',
 'Normal': 'black',
    'Normal-like' : 'brown'}

# Drop brca subtype column
features = features.drop(columns=['brca_subtype'])

# Apply PCA
pca = PCA(n_components = 2)
pca.fit(features)
df_pca = pd.DataFrame(pca.transform(features), columns=['PC1', 'PC2'], index=features.index)
plt.scatter(df_pca['PC1'], df_pca['PC2'],
            color=[color_map[sampleId_to_brca_subtype_map[i]] for i in df_pca.index.tolist()])

# Add the axis labels
plt.xlabel('PC 1 (%.2f%%)' % (pca.explained_variance_ratio_[0]*100))
plt.ylabel('PC 2 (%.2f%%)' % (pca.explained_variance_ratio_[1]*100))

handlelist = [plt.plot([], marker="o", ls="", color=color)[0] for color in color_map.values()]
plt.legend(handlelist,color_map.keys(),bbox_to_anchor=(1,1), loc="upper left")
plt.savefig('./Plots/PCAwithoutNaN.png')

plt.show()
```


![png](images/output_27_0.png)


### Analysis on Phantusus
#### Sort the gene_exp_T using brca_subtype then remove the column before taking transpose
We want genes in rows and samples in columns


```python
gct_df = gene_exp_T.sort_values(by ='brca_subtype').drop(columns=['brca_subtype']).T
```
#### Make a GCToo object and save it as gct file which can be inputted on phantusus directly


```python
# Need to make a GCToo object to save it as a gct file
from cmapPy.pandasGEXpress.GCToo import GCToo
from cmapPy.pandasGEXpress.write_gct import write
```

```python
metadata = gene_exp_T[['brca_subtype']]
gct_Too_obj = GCToo(gct_df, col_metadata_df= metadata)
write(gct_Too_obj, './Generated Files/gene_exp_data.gct')
```
GCT Analysis Results : http://52.9.27.18:8000/?session=x0edca8d11529aa

### Normalization Check

#### 1. Checking if gene expression Levels are normalized across all samples


```python
import seaborn as sns

fig, ax = plt.subplots(figsize=(50,15))    
ax = sns.boxplot(data=gene_exp_T,  order=gene_exp_T.columns.tolist()[:-1])
```


![png](images/output_36_0.png)


#### 2. Checking if Expression Levels in each sample is normalized on raw data

Checking if gene expression levels in each sample is normalized


```python
fig, ax = plt.subplots(figsize=(50,15)) 
ax = sns.boxplot( data=gene_exp_df_mod.set_index('sample'), order=gene_exp_df_mod.set_index('sample').columns.tolist()[:50])
```


![png](images/output_39_0.png)


<B> Note : </B> <BR>
The x axis values in the above plot are samples(20 out of 1218) whereas the y axis denotes legends

#### 3. Performing Cyclic Loess normalization in R


```python
# Save your gene_exp_df_mod and meta_data in csv files to run the function in R
df = gene_exp_df_mod.set_index('sample')
df = df.astype(int)
df.to_csv('./Generated Files/gene_exp_PAM50_BC.csv')

meta = meta_data[['PAM50Call_RNAseq']]
meta = meta.reset_index()[meta.reset_index()['sampleID'].isin(gene_exp_df.columns.tolist()[1:])].set_index('sampleID')
meta = meta.replace(np.nan, 'missing', regex=True)
meta[['PAM50Call_RNAseq']].to_csv('./Generated Files/metadata_R.csv')
```

```python
# R code
library(DESeq2)
```

```python
counts_Data <- read.csv("./Generated Files/gene_exp_PAM50_BC.csv", header=TRUE, row.names="sample")
metadata <- read.csv("./Generated Files/metadata_R.csv")
```

```python
# Preparing for DESeq Data
dataset2 = DESeqDataSetFromMatrix(countData = counts_Data, colData = metadata, design = ~1)
# data_vst <- vst(dataset2, nsub = nrow(dataset2), blind = FALSE, fitType = "mean")

# As data Points are less we use fitType = 'mean' 
data_variance_st = varianceStabilizingTransformation(dataset2, blind = FALSE, fitType = "mean")
```

```python
boxplot(assay(data_variance_st)[, 1:20])
```


![png](images/output_46_0.png)


#### 4. Normalization using mean and standard deviation (x - mean)/std


```python
df = gene_exp_df_mod.set_index('sample')
normalized_df = (df - df.mean())/df.std()
```

```python
normalized_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>TCGA-AR-A5QQ-01</th>
      <th>TCGA-D8-A1JA-01</th>
      <th>TCGA-BH-A0BQ-01</th>
      <th>TCGA-BH-A0BT-01</th>
      <th>TCGA-A8-A06X-01</th>
      <th>TCGA-A8-A096-01</th>
      <th>TCGA-BH-A0C7-01</th>
      <th>TCGA-AC-A5XU-01</th>
      <th>TCGA-PE-A5DE-01</th>
      <th>TCGA-PE-A5DC-01</th>
      <th>...</th>
      <th>TCGA-A7-A13E-11</th>
      <th>TCGA-C8-A8HP-01</th>
      <th>TCGA-E9-A5FL-01</th>
      <th>TCGA-AC-A2FB-11</th>
      <th>TCGA-E2-A15F-01</th>
      <th>TCGA-A2-A3XT-01</th>
      <th>TCGA-B6-A0X7-01</th>
      <th>TCGA-BH-A1EV-11</th>
      <th>TCGA-3C-AALJ-01</th>
      <th>TCGA-B6-A0X1-01</th>
    </tr>
    <tr>
      <th>sample</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>KNTC2</th>
      <td>-0.449902</td>
      <td>-0.477033</td>
      <td>-1.076396</td>
      <td>-1.054310</td>
      <td>-0.334276</td>
      <td>-0.252223</td>
      <td>-0.574993</td>
      <td>-0.770679</td>
      <td>-0.959573</td>
      <td>-0.570089</td>
      <td>...</td>
      <td>-1.150448</td>
      <td>-0.407846</td>
      <td>-0.189798</td>
      <td>-0.995239</td>
      <td>-0.684561</td>
      <td>-0.170545</td>
      <td>-0.975355</td>
      <td>-1.112027</td>
      <td>-0.027358</td>
      <td>-0.106460</td>
    </tr>
    <tr>
      <th>ERBB2</th>
      <td>0.293686</td>
      <td>2.376099</td>
      <td>1.755299</td>
      <td>2.138959</td>
      <td>1.225761</td>
      <td>1.329792</td>
      <td>1.428888</td>
      <td>1.307683</td>
      <td>1.227781</td>
      <td>1.661560</td>
      <td>...</td>
      <td>1.202738</td>
      <td>3.335244</td>
      <td>0.925343</td>
      <td>1.150784</td>
      <td>1.370924</td>
      <td>0.507178</td>
      <td>1.793299</td>
      <td>1.846674</td>
      <td>1.479793</td>
      <td>1.218924</td>
    </tr>
    <tr>
      <th>TMEM45B</th>
      <td>-1.478486</td>
      <td>1.246397</td>
      <td>-0.536295</td>
      <td>-0.638179</td>
      <td>-2.251139</td>
      <td>0.260667</td>
      <td>-2.503304</td>
      <td>-1.099309</td>
      <td>-0.829544</td>
      <td>-1.853501</td>
      <td>...</td>
      <td>-0.583720</td>
      <td>-0.199792</td>
      <td>-2.118073</td>
      <td>-0.592053</td>
      <td>-2.880298</td>
      <td>-3.495972</td>
      <td>-0.465027</td>
      <td>-0.663388</td>
      <td>-2.109928</td>
      <td>-2.507838</td>
    </tr>
    <tr>
      <th>KRT14</th>
      <td>3.162262</td>
      <td>-2.786618</td>
      <td>1.102969</td>
      <td>0.649187</td>
      <td>-2.135060</td>
      <td>-2.703213</td>
      <td>-0.360294</td>
      <td>0.075157</td>
      <td>0.423340</td>
      <td>0.860982</td>
      <td>...</td>
      <td>1.280468</td>
      <td>-1.448238</td>
      <td>0.699508</td>
      <td>2.121107</td>
      <td>0.308525</td>
      <td>0.263339</td>
      <td>0.549053</td>
      <td>1.647241</td>
      <td>-2.374357</td>
      <td>0.298156</td>
    </tr>
    <tr>
      <th>BCL2</th>
      <td>-0.879070</td>
      <td>-0.667748</td>
      <td>0.774104</td>
      <td>-0.667652</td>
      <td>0.118188</td>
      <td>0.947248</td>
      <td>0.447669</td>
      <td>1.348569</td>
      <td>0.391660</td>
      <td>0.376275</td>
      <td>...</td>
      <td>1.099745</td>
      <td>-0.783449</td>
      <td>-0.966599</td>
      <td>0.880094</td>
      <td>0.889847</td>
      <td>-0.811361</td>
      <td>1.208060</td>
      <td>0.903341</td>
      <td>0.770272</td>
      <td>-0.167232</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 1218 columns</p>
</div>




```python
fig, ax = plt.subplots(figsize=(50,15)) 
ax = sns.boxplot( data=normalized_df, order=normalized_df.columns.tolist()[:50])
```


![png](images/output_50_0.png)
