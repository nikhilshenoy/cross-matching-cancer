---
authors:
- nikhil.shenoy@elucidata.io
created_at: 2019-05-29 00:00:00
tags:
- Clustering, PCA
- GCT Analysis
thumbnail: images/output_9_0.png
title: Analysis of Breast Cancer (METABRIC, Nature 2012 & Nat Commun 2016)
tldr: Analysis of a different BC dataset compared to TCGA to see if the clustering
  takes place. Also to study the gene expression levels
updated_at: 2019-06-12 11:12:22.725576
---
### Importing Necessary Modules


```python
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import seaborn as sns
```
### Data Retrieval

#### Getting Gene Expression Data


```python
gene_exp_df = pd.read_csv('./brca_metabric/data_expression_median.txt', sep = '\t')
```

```python
# Shape of the main data
gene_exp_df.shape
```




    (24368, 1906)




```python
# Basically under the Hugo_Symbol we have the genes
gene_exp_df.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Hugo_Symbol</th>
      <th>Entrez_Gene_Id</th>
      <th>MB-0362</th>
      <th>MB-0346</th>
      <th>MB-0386</th>
      <th>MB-0574</th>
      <th>MB-0503</th>
      <th>MB-0641</th>
      <th>MB-0201</th>
      <th>MB-0218</th>
      <th>...</th>
      <th>MB-6122</th>
      <th>MB-6192</th>
      <th>MB-4820</th>
      <th>MB-5527</th>
      <th>MB-5167</th>
      <th>MB-5465</th>
      <th>MB-5453</th>
      <th>MB-5471</th>
      <th>MB-5127</th>
      <th>MB-4313</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>RERE</td>
      <td>473.0</td>
      <td>8.676978</td>
      <td>9.653589</td>
      <td>9.033589</td>
      <td>8.814855</td>
      <td>9.274265</td>
      <td>9.286585</td>
      <td>8.437347</td>
      <td>8.569973</td>
      <td>...</td>
      <td>8.756024</td>
      <td>8.804947</td>
      <td>9.991215</td>
      <td>9.595923</td>
      <td>9.637249</td>
      <td>8.131637</td>
      <td>9.606915</td>
      <td>9.049296</td>
      <td>8.858622</td>
      <td>8.415867</td>
    </tr>
    <tr>
      <th>1</th>
      <td>RNF165</td>
      <td>494470.0</td>
      <td>6.075331</td>
      <td>6.687887</td>
      <td>5.910885</td>
      <td>5.628740</td>
      <td>5.908698</td>
      <td>6.206729</td>
      <td>6.095592</td>
      <td>6.383530</td>
      <td>...</td>
      <td>7.101087</td>
      <td>5.601472</td>
      <td>7.103160</td>
      <td>6.418987</td>
      <td>6.203370</td>
      <td>9.101942</td>
      <td>7.427494</td>
      <td>6.850000</td>
      <td>6.550450</td>
      <td>6.831722</td>
    </tr>
    <tr>
      <th>2</th>
      <td>CD049690</td>
      <td>NaN</td>
      <td>5.453928</td>
      <td>5.454185</td>
      <td>5.501577</td>
      <td>5.471941</td>
      <td>5.531743</td>
      <td>5.372668</td>
      <td>5.693519</td>
      <td>5.401276</td>
      <td>...</td>
      <td>5.344674</td>
      <td>5.364348</td>
      <td>5.504866</td>
      <td>5.277314</td>
      <td>5.439207</td>
      <td>5.423027</td>
      <td>5.534115</td>
      <td>5.339346</td>
      <td>5.566071</td>
      <td>5.541395</td>
    </tr>
    <tr>
      <th>3</th>
      <td>BC033982</td>
      <td>NaN</td>
      <td>4.994525</td>
      <td>5.346010</td>
      <td>5.247467</td>
      <td>5.316523</td>
      <td>5.244094</td>
      <td>5.167365</td>
      <td>5.189106</td>
      <td>5.416517</td>
      <td>...</td>
      <td>5.300466</td>
      <td>4.861101</td>
      <td>4.980904</td>
      <td>5.436855</td>
      <td>5.346776</td>
      <td>4.939292</td>
      <td>5.062191</td>
      <td>5.166765</td>
      <td>5.140141</td>
      <td>5.266802</td>
    </tr>
    <tr>
      <th>4</th>
      <td>PHF7</td>
      <td>51533.0</td>
      <td>5.838270</td>
      <td>5.600876</td>
      <td>6.030718</td>
      <td>5.849428</td>
      <td>5.964661</td>
      <td>5.783374</td>
      <td>5.737572</td>
      <td>5.923928</td>
      <td>...</td>
      <td>5.653481</td>
      <td>5.922894</td>
      <td>6.181493</td>
      <td>5.992153</td>
      <td>6.230316</td>
      <td>5.644587</td>
      <td>5.927409</td>
      <td>6.117095</td>
      <td>5.936371</td>
      <td>7.408960</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 1906 columns</p>
</div>



<B> Check if Gene Expression Data is normalized </B>


```python
df = gene_exp_df.iloc[:, 2:]
fig, ax = plt.subplots(figsize=(50,15))    
ax = sns.boxplot(data=df,  order=df.columns.tolist()[1:10])
```


![png](images/output_9_0.png)


<B> Inference </B> <BR>
The gene expression values in each sample is normalized using median normalization and it is log2 scaled!

#### Getting meta_data of Gene Expression Data


```python
meta_data = pd.read_csv('./brca_metabric/meta_expression_median.txt', sep = ': ', engine = 'python')
```

```python
meta_data
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>cancer_study_identifier</th>
      <th>brca_metabric</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>genetic_alteration_type</td>
      <td>MRNA_EXPRESSION</td>
    </tr>
    <tr>
      <th>1</th>
      <td>datatype</td>
      <td>CONTINUOUS</td>
    </tr>
    <tr>
      <th>2</th>
      <td>stable_id</td>
      <td>mrna</td>
    </tr>
    <tr>
      <th>3</th>
      <td>show_profile_in_analysis_tab</td>
      <td>false</td>
    </tr>
    <tr>
      <th>4</th>
      <td>profile_name</td>
      <td>mRNA expression (microarray)</td>
    </tr>
    <tr>
      <th>5</th>
      <td>profile_description</td>
      <td>Expression log intensity levels (Illumina Huma...</td>
    </tr>
    <tr>
      <th>6</th>
      <td>data_filename</td>
      <td>data_expression_median.txt</td>
    </tr>
  </tbody>
</table>
</div>



#### Getting data about different cancer samples


```python
data_clinical_patient = pd.read_csv('./brca_metabric/data_clinical_patient.txt', sep='\t')
sample_data = pd.DataFrame(data=data_clinical_patient.iloc[4:])
```
Basically the data_clinically_patient dataframe has some information about the columns in the first 3 rows. So, I have created a new dataframe <B> sample_data </B> from the data_clinical_patient dataframe


```python
data_clinical_patient.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>#Patient Identifier</th>
      <th>Lymph nodes examined positive</th>
      <th>Nottingham prognostic index</th>
      <th>Cellularity</th>
      <th>Chemotherapy</th>
      <th>Cohort</th>
      <th>ER status measured by IHC</th>
      <th>HER2 status measured by SNP6</th>
      <th>Hormone Therapy</th>
      <th>Inferred Menopausal State</th>
      <th>...</th>
      <th>Age at Diagnosis</th>
      <th>Overall Survival (Months)</th>
      <th>Overall Survival Status</th>
      <th>Pam50 + Claudin-low subtype</th>
      <th>3-Gene classifier subtype</th>
      <th>Patient's Vital Status</th>
      <th>Primary Tumor Laterality</th>
      <th>Radio Therapy</th>
      <th>Tumor Other Histologic Subtype</th>
      <th>Type of Breast Surgery</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>#Identifier to uniquely specify a patient.</td>
      <td>Number of lymphnodes positive</td>
      <td>Nottingham prognostic index</td>
      <td>Tumor Content</td>
      <td>Chemotherapy.</td>
      <td>Cohort.</td>
      <td>ER status measured by IHC</td>
      <td>HER2 status measured by SNP6</td>
      <td>Hormone Therapy</td>
      <td>Inferred Menopausal State</td>
      <td>...</td>
      <td>Age at Diagnosis</td>
      <td>Overall survival in months since initial diago...</td>
      <td>Overall patient survival status.</td>
      <td>Pam50 + Claudin-low subtype</td>
      <td>3-Gene classifier subtype</td>
      <td>The survival state of the person.</td>
      <td>For tumors in paired organs, designates the si...</td>
      <td>Radio Therapy</td>
      <td>Text to describe a tumor's histologic subtype ...</td>
      <td>Type of Breast Surgery</td>
    </tr>
    <tr>
      <th>1</th>
      <td>#STRING</td>
      <td>STRING</td>
      <td>NUMBER</td>
      <td>STRING</td>
      <td>STRING</td>
      <td>STRING</td>
      <td>STRING</td>
      <td>STRING</td>
      <td>STRING</td>
      <td>STRING</td>
      <td>...</td>
      <td>NUMBER</td>
      <td>NUMBER</td>
      <td>STRING</td>
      <td>STRING</td>
      <td>STRING</td>
      <td>STRING</td>
      <td>STRING</td>
      <td>STRING</td>
      <td>STRING</td>
      <td>STRING</td>
    </tr>
    <tr>
      <th>2</th>
      <td>#1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>...</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
    </tr>
    <tr>
      <th>3</th>
      <td>PATIENT_ID</td>
      <td>LYMPH_NODES_EXAMINED_POSITIVE</td>
      <td>NPI</td>
      <td>CELLULARITY</td>
      <td>CHEMOTHERAPY</td>
      <td>COHORT</td>
      <td>ER_IHC</td>
      <td>HER2_SNP6</td>
      <td>HORMONE_THERAPY</td>
      <td>INFERRED_MENOPAUSAL_STATE</td>
      <td>...</td>
      <td>AGE_AT_DIAGNOSIS</td>
      <td>OS_MONTHS</td>
      <td>OS_STATUS</td>
      <td>CLAUDIN_SUBTYPE</td>
      <td>THREEGENE</td>
      <td>VITAL_STATUS</td>
      <td>LATERALITY</td>
      <td>RADIO_THERAPY</td>
      <td>HISTOLOGICAL_SUBTYPE</td>
      <td>BREAST_SURGERY</td>
    </tr>
    <tr>
      <th>4</th>
      <td>MB-0000</td>
      <td>10</td>
      <td>6.044</td>
      <td>NaN</td>
      <td>NO</td>
      <td>1</td>
      <td>Positve</td>
      <td>NEUTRAL</td>
      <td>YES</td>
      <td>Post</td>
      <td>...</td>
      <td>75.65</td>
      <td>140.5</td>
      <td>LIVING</td>
      <td>claudin-low</td>
      <td>ER-/HER2-</td>
      <td>Living</td>
      <td>Right</td>
      <td>YES</td>
      <td>Ductal/NST</td>
      <td>MASTECTOMY</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 21 columns</p>
</div>




```python
sample_data.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>#Patient Identifier</th>
      <th>Lymph nodes examined positive</th>
      <th>Nottingham prognostic index</th>
      <th>Cellularity</th>
      <th>Chemotherapy</th>
      <th>Cohort</th>
      <th>ER status measured by IHC</th>
      <th>HER2 status measured by SNP6</th>
      <th>Hormone Therapy</th>
      <th>Inferred Menopausal State</th>
      <th>...</th>
      <th>Age at Diagnosis</th>
      <th>Overall Survival (Months)</th>
      <th>Overall Survival Status</th>
      <th>Pam50 + Claudin-low subtype</th>
      <th>3-Gene classifier subtype</th>
      <th>Patient's Vital Status</th>
      <th>Primary Tumor Laterality</th>
      <th>Radio Therapy</th>
      <th>Tumor Other Histologic Subtype</th>
      <th>Type of Breast Surgery</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>4</th>
      <td>MB-0000</td>
      <td>10</td>
      <td>6.044</td>
      <td>NaN</td>
      <td>NO</td>
      <td>1</td>
      <td>Positve</td>
      <td>NEUTRAL</td>
      <td>YES</td>
      <td>Post</td>
      <td>...</td>
      <td>75.65</td>
      <td>140.5</td>
      <td>LIVING</td>
      <td>claudin-low</td>
      <td>ER-/HER2-</td>
      <td>Living</td>
      <td>Right</td>
      <td>YES</td>
      <td>Ductal/NST</td>
      <td>MASTECTOMY</td>
    </tr>
    <tr>
      <th>5</th>
      <td>MB-0002</td>
      <td>0</td>
      <td>4.02</td>
      <td>High</td>
      <td>NO</td>
      <td>1</td>
      <td>Positve</td>
      <td>NEUTRAL</td>
      <td>YES</td>
      <td>Pre</td>
      <td>...</td>
      <td>43.19</td>
      <td>84.63333333</td>
      <td>LIVING</td>
      <td>LumA</td>
      <td>ER+/HER2- High Prolif</td>
      <td>Living</td>
      <td>Right</td>
      <td>YES</td>
      <td>Ductal/NST</td>
      <td>BREAST CONSERVING</td>
    </tr>
    <tr>
      <th>6</th>
      <td>MB-0005</td>
      <td>1</td>
      <td>4.03</td>
      <td>High</td>
      <td>YES</td>
      <td>1</td>
      <td>Positve</td>
      <td>NEUTRAL</td>
      <td>YES</td>
      <td>Pre</td>
      <td>...</td>
      <td>48.87</td>
      <td>163.7</td>
      <td>DECEASED</td>
      <td>LumB</td>
      <td>NaN</td>
      <td>Died of Disease</td>
      <td>Right</td>
      <td>NO</td>
      <td>Ductal/NST</td>
      <td>MASTECTOMY</td>
    </tr>
    <tr>
      <th>7</th>
      <td>MB-0006</td>
      <td>3</td>
      <td>4.05</td>
      <td>Moderate</td>
      <td>YES</td>
      <td>1</td>
      <td>Positve</td>
      <td>NEUTRAL</td>
      <td>YES</td>
      <td>Pre</td>
      <td>...</td>
      <td>47.68</td>
      <td>164.9333333</td>
      <td>LIVING</td>
      <td>LumB</td>
      <td>NaN</td>
      <td>Living</td>
      <td>Right</td>
      <td>YES</td>
      <td>Mixed</td>
      <td>MASTECTOMY</td>
    </tr>
    <tr>
      <th>8</th>
      <td>MB-0008</td>
      <td>8</td>
      <td>6.08</td>
      <td>High</td>
      <td>YES</td>
      <td>1</td>
      <td>Positve</td>
      <td>NEUTRAL</td>
      <td>YES</td>
      <td>Post</td>
      <td>...</td>
      <td>76.97</td>
      <td>41.36666667</td>
      <td>DECEASED</td>
      <td>LumB</td>
      <td>ER+/HER2- High Prolif</td>
      <td>Died of Disease</td>
      <td>Right</td>
      <td>YES</td>
      <td>Mixed</td>
      <td>MASTECTOMY</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 21 columns</p>
</div>




```python
sample_data['Pam50 + Claudin-low subtype'].isna().sum()
```




    529



<B> Note : </B> <BR>
The 'Pam50 + Claudin-low subtype' column has PAM50 classification for each sample type (Total number of NaN values is just 529 out of 2513 rows) 

### Subsetting the gene expression data and sample data
1. We subset the sample data to necessary columns to get a mapping of the sample to brca subtype
2. We subset gene expresion data to only the 50 genes using PAM50

#### Subsetting the sample data to necessary columns


```python
brca_subtype_mapping = pd.DataFrame(sample_data[['#Patient Identifier' ,'Pam50 + Claudin-low subtype']]).set_index('#Patient Identifier')
brca_subtype_mapping['Pam50 + Claudin-low subtype'] = brca_subtype_mapping['Pam50 + Claudin-low subtype'].replace('Normal', 'Normal-like') 
brca_subtype_mapping.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Pam50 + Claudin-low subtype</th>
    </tr>
    <tr>
      <th>#Patient Identifier</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>MB-0000</th>
      <td>claudin-low</td>
    </tr>
    <tr>
      <th>MB-0002</th>
      <td>LumA</td>
    </tr>
    <tr>
      <th>MB-0005</th>
      <td>LumB</td>
    </tr>
    <tr>
      <th>MB-0006</th>
      <td>LumB</td>
    </tr>
    <tr>
      <th>MB-0008</th>
      <td>LumB</td>
    </tr>
  </tbody>
</table>
</div>



<B> Note : </B> <BR>
This data frame also tells if the sample is claudin low subtype 


```python
brca_subtype_mapping['Pam50 + Claudin-low subtype'].unique()
```




    array(['claudin-low', 'LumA', 'LumB', 'Normal-like', nan, 'Her2', 'Basal',
           'NC'], dtype=object)



#### Subsetting the entire dataset using PAM50 gene set


```python
total_genes = gene_exp_df['Hugo_Symbol'].tolist()
PAM50_gene_df = pd.read_csv('/home/rstudio/PAM50/bioclassifier_R/pam50_centroids.txt', sep = '\t')
PAM50_gene_list = PAM50_gene_df.iloc[:,0].tolist()
[i for i in PAM50_gene_list if i not in total_genes]
```




    ['BAG1', 'CDCA1', 'GPR160', 'KNTC2', 'MIA', 'ORC6L', 'TMEM45B']



Therefore, these genes might be present under a different alias in the gene_expression_data


```python
# Some minor changes as alias of 2 genes is present in PAM50 gene set
# ('NUF2' to 'CDCA1') ('NDC80' to 'KNTC2') ('ORC6L' to 'ORC6')('MIA-RAB4B' to 'MIA')
gene_exp_df['Hugo_Symbol'] = gene_exp_df['Hugo_Symbol'].str.replace('NUF2','CDCA1')
gene_exp_df['Hugo_Symbol'] = gene_exp_df['Hugo_Symbol'].str.replace('NDC80','KNTC2')
gene_exp_df['Hugo_Symbol'] = gene_exp_df['Hugo_Symbol'].str.replace('ORC6','ORC6L')
gene_exp_df['Hugo_Symbol'] = gene_exp_df['Hugo_Symbol'].str.replace('MIA-RAB4B','MIA')
```

```python
total_genes = gene_exp_df['Hugo_Symbol'].tolist()
[i for i in PAM50_gene_list if i not in total_genes]
```




    ['BAG1', 'GPR160', 'TMEM45B']



### Creating a modified Gene Expression Data (with PAM50 data only)


```python
gene_exp_df_mod = gene_exp_df[gene_exp_df['Hugo_Symbol'].isin(PAM50_gene_list)]
print (gene_exp_df_mod.shape)
```
    (47, 1906)



```python
gene_exp_df_mod.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Hugo_Symbol</th>
      <th>Entrez_Gene_Id</th>
      <th>MB-0362</th>
      <th>MB-0346</th>
      <th>MB-0386</th>
      <th>MB-0574</th>
      <th>MB-0503</th>
      <th>MB-0641</th>
      <th>MB-0201</th>
      <th>MB-0218</th>
      <th>...</th>
      <th>MB-6122</th>
      <th>MB-6192</th>
      <th>MB-4820</th>
      <th>MB-5527</th>
      <th>MB-5167</th>
      <th>MB-5465</th>
      <th>MB-5453</th>
      <th>MB-5471</th>
      <th>MB-5127</th>
      <th>MB-4313</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>154</th>
      <td>MKI67</td>
      <td>4288.0</td>
      <td>5.764969</td>
      <td>6.005284</td>
      <td>5.722205</td>
      <td>5.849332</td>
      <td>5.929166</td>
      <td>5.487885</td>
      <td>5.898539</td>
      <td>5.703781</td>
      <td>...</td>
      <td>5.946379</td>
      <td>5.746123</td>
      <td>5.942180</td>
      <td>6.247307</td>
      <td>6.341698</td>
      <td>5.877959</td>
      <td>5.603806</td>
      <td>5.847104</td>
      <td>5.841964</td>
      <td>5.367544</td>
    </tr>
    <tr>
      <th>474</th>
      <td>FGFR4</td>
      <td>2264.0</td>
      <td>5.643984</td>
      <td>10.917248</td>
      <td>5.535386</td>
      <td>6.034698</td>
      <td>5.682765</td>
      <td>5.819236</td>
      <td>6.503140</td>
      <td>7.339241</td>
      <td>...</td>
      <td>5.563413</td>
      <td>5.586380</td>
      <td>6.000012</td>
      <td>6.194052</td>
      <td>5.449482</td>
      <td>5.676891</td>
      <td>6.274398</td>
      <td>5.801690</td>
      <td>5.839152</td>
      <td>6.038270</td>
    </tr>
    <tr>
      <th>805</th>
      <td>MELK</td>
      <td>9833.0</td>
      <td>8.012247</td>
      <td>8.258717</td>
      <td>8.042688</td>
      <td>7.045507</td>
      <td>6.632858</td>
      <td>5.869465</td>
      <td>8.177807</td>
      <td>7.638726</td>
      <td>...</td>
      <td>7.961351</td>
      <td>7.734259</td>
      <td>6.294439</td>
      <td>7.853622</td>
      <td>7.645557</td>
      <td>8.429562</td>
      <td>5.886021</td>
      <td>6.756907</td>
      <td>6.991321</td>
      <td>5.522176</td>
    </tr>
    <tr>
      <th>905</th>
      <td>PHGDH</td>
      <td>26227.0</td>
      <td>10.931677</td>
      <td>11.332236</td>
      <td>8.367386</td>
      <td>9.448428</td>
      <td>8.779597</td>
      <td>9.793860</td>
      <td>11.651443</td>
      <td>8.744625</td>
      <td>...</td>
      <td>10.451546</td>
      <td>10.503167</td>
      <td>6.599272</td>
      <td>9.366526</td>
      <td>10.120230</td>
      <td>11.931184</td>
      <td>10.840565</td>
      <td>10.557190</td>
      <td>9.309382</td>
      <td>9.584985</td>
    </tr>
    <tr>
      <th>1630</th>
      <td>CDC6</td>
      <td>990.0</td>
      <td>5.562816</td>
      <td>7.164704</td>
      <td>5.641819</td>
      <td>5.387240</td>
      <td>5.464620</td>
      <td>5.391536</td>
      <td>5.567559</td>
      <td>6.045137</td>
      <td>...</td>
      <td>5.570230</td>
      <td>5.506540</td>
      <td>5.366131</td>
      <td>5.353542</td>
      <td>5.549981</td>
      <td>5.354009</td>
      <td>5.274143</td>
      <td>5.413610</td>
      <td>5.614423</td>
      <td>5.442065</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 1906 columns</p>
</div>



Saving the modified dataframe as a csv


```python
gene_exp_df_mod.to_csv('./Generated Files/gene_exp_metabrics_mod.csv')
```
### Preparing data for PCA
gen_exp_T is the transposed data and we want to create a new column for brca_subtypes and remove 'Entrez_Gene_Id' as one identifier is enough


```python
# Some formatting to match our samples with metadata
gene_exp_T = gene_exp_df_mod.drop(columns = ['Entrez_Gene_Id']).set_index('Hugo_Symbol').T
gene_exp_T['brca_subtype'] = np.nan
gene_exp_T.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th>Hugo_Symbol</th>
      <th>MKI67</th>
      <th>FGFR4</th>
      <th>MELK</th>
      <th>PHGDH</th>
      <th>CDC6</th>
      <th>RRM2</th>
      <th>MDM2</th>
      <th>FOXA1</th>
      <th>CXXC5</th>
      <th>KNTC2</th>
      <th>...</th>
      <th>TYMS</th>
      <th>PGR</th>
      <th>NAT1</th>
      <th>ANLN</th>
      <th>SFRP1</th>
      <th>MIA</th>
      <th>CEP55</th>
      <th>CDH3</th>
      <th>SLC39A6</th>
      <th>brca_subtype</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>MB-0362</th>
      <td>5.764969</td>
      <td>5.643984</td>
      <td>8.012247</td>
      <td>10.931677</td>
      <td>5.562816</td>
      <td>6.633174</td>
      <td>5.972979</td>
      <td>11.873942</td>
      <td>10.525354</td>
      <td>5.638313</td>
      <td>...</td>
      <td>9.142547</td>
      <td>6.120169</td>
      <td>10.628267</td>
      <td>5.796464</td>
      <td>7.952700</td>
      <td>5.721733</td>
      <td>6.774027</td>
      <td>6.660659</td>
      <td>10.567694</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>MB-0346</th>
      <td>6.005284</td>
      <td>10.917248</td>
      <td>8.258717</td>
      <td>11.332236</td>
      <td>7.164704</td>
      <td>7.788523</td>
      <td>5.554918</td>
      <td>10.829918</td>
      <td>10.675504</td>
      <td>6.008042</td>
      <td>...</td>
      <td>9.674746</td>
      <td>5.291619</td>
      <td>6.639451</td>
      <td>6.333347</td>
      <td>6.673074</td>
      <td>5.510294</td>
      <td>7.747693</td>
      <td>8.200274</td>
      <td>7.420700</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>MB-0386</th>
      <td>5.722205</td>
      <td>5.535386</td>
      <td>8.042688</td>
      <td>8.367386</td>
      <td>5.641819</td>
      <td>6.330656</td>
      <td>5.631906</td>
      <td>11.417427</td>
      <td>10.059221</td>
      <td>5.636172</td>
      <td>...</td>
      <td>7.518175</td>
      <td>7.416926</td>
      <td>10.330668</td>
      <td>6.780698</td>
      <td>9.116463</td>
      <td>5.995795</td>
      <td>7.093586</td>
      <td>6.124498</td>
      <td>10.211003</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>MB-0574</th>
      <td>5.849332</td>
      <td>6.034698</td>
      <td>7.045507</td>
      <td>9.448428</td>
      <td>5.387240</td>
      <td>5.994098</td>
      <td>5.706261</td>
      <td>11.266299</td>
      <td>10.432546</td>
      <td>5.736326</td>
      <td>...</td>
      <td>8.162441</td>
      <td>7.313249</td>
      <td>9.170203</td>
      <td>5.828678</td>
      <td>6.545720</td>
      <td>5.741421</td>
      <td>6.900706</td>
      <td>6.182236</td>
      <td>10.929505</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>MB-0503</th>
      <td>5.929166</td>
      <td>5.682765</td>
      <td>6.632858</td>
      <td>8.779597</td>
      <td>5.464620</td>
      <td>5.631839</td>
      <td>5.568084</td>
      <td>11.044143</td>
      <td>10.992162</td>
      <td>5.398625</td>
      <td>...</td>
      <td>8.322062</td>
      <td>7.450691</td>
      <td>8.658371</td>
      <td>5.495414</td>
      <td>8.656046</td>
      <td>6.389019</td>
      <td>6.657073</td>
      <td>8.043682</td>
      <td>10.209835</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 48 columns</p>
</div>




```python
gene_exp_T['brca_subtype'] = gene_exp_T['brca_subtype'].combine_first(brca_subtype_mapping['Pam50 + Claudin-low subtype'])
print (str(gene_exp_T['brca_subtype'].isna().sum()) + " : samples that are nan out of " +
       str(len(gene_exp_T)))
```
    0 : samples that are nan out of 1904



```python
# Save this data as PCA_transposed_df.csv
gene_exp_T.to_csv('./Generated Files/gene_exp_metabrics_T.csv')
gene_exp_T.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th>Hugo_Symbol</th>
      <th>MKI67</th>
      <th>FGFR4</th>
      <th>MELK</th>
      <th>PHGDH</th>
      <th>CDC6</th>
      <th>RRM2</th>
      <th>MDM2</th>
      <th>FOXA1</th>
      <th>CXXC5</th>
      <th>KNTC2</th>
      <th>...</th>
      <th>TYMS</th>
      <th>PGR</th>
      <th>NAT1</th>
      <th>ANLN</th>
      <th>SFRP1</th>
      <th>MIA</th>
      <th>CEP55</th>
      <th>CDH3</th>
      <th>SLC39A6</th>
      <th>brca_subtype</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>MB-0362</th>
      <td>5.764969</td>
      <td>5.643984</td>
      <td>8.012247</td>
      <td>10.931677</td>
      <td>5.562816</td>
      <td>6.633174</td>
      <td>5.972979</td>
      <td>11.873942</td>
      <td>10.525354</td>
      <td>5.638313</td>
      <td>...</td>
      <td>9.142547</td>
      <td>6.120169</td>
      <td>10.628267</td>
      <td>5.796464</td>
      <td>7.952700</td>
      <td>5.721733</td>
      <td>6.774027</td>
      <td>6.660659</td>
      <td>10.567694</td>
      <td>LumA</td>
    </tr>
    <tr>
      <th>MB-0346</th>
      <td>6.005284</td>
      <td>10.917248</td>
      <td>8.258717</td>
      <td>11.332236</td>
      <td>7.164704</td>
      <td>7.788523</td>
      <td>5.554918</td>
      <td>10.829918</td>
      <td>10.675504</td>
      <td>6.008042</td>
      <td>...</td>
      <td>9.674746</td>
      <td>5.291619</td>
      <td>6.639451</td>
      <td>6.333347</td>
      <td>6.673074</td>
      <td>5.510294</td>
      <td>7.747693</td>
      <td>8.200274</td>
      <td>7.420700</td>
      <td>Her2</td>
    </tr>
    <tr>
      <th>MB-0386</th>
      <td>5.722205</td>
      <td>5.535386</td>
      <td>8.042688</td>
      <td>8.367386</td>
      <td>5.641819</td>
      <td>6.330656</td>
      <td>5.631906</td>
      <td>11.417427</td>
      <td>10.059221</td>
      <td>5.636172</td>
      <td>...</td>
      <td>7.518175</td>
      <td>7.416926</td>
      <td>10.330668</td>
      <td>6.780698</td>
      <td>9.116463</td>
      <td>5.995795</td>
      <td>7.093586</td>
      <td>6.124498</td>
      <td>10.211003</td>
      <td>LumA</td>
    </tr>
    <tr>
      <th>MB-0574</th>
      <td>5.849332</td>
      <td>6.034698</td>
      <td>7.045507</td>
      <td>9.448428</td>
      <td>5.387240</td>
      <td>5.994098</td>
      <td>5.706261</td>
      <td>11.266299</td>
      <td>10.432546</td>
      <td>5.736326</td>
      <td>...</td>
      <td>8.162441</td>
      <td>7.313249</td>
      <td>9.170203</td>
      <td>5.828678</td>
      <td>6.545720</td>
      <td>5.741421</td>
      <td>6.900706</td>
      <td>6.182236</td>
      <td>10.929505</td>
      <td>LumA</td>
    </tr>
    <tr>
      <th>MB-0503</th>
      <td>5.929166</td>
      <td>5.682765</td>
      <td>6.632858</td>
      <td>8.779597</td>
      <td>5.464620</td>
      <td>5.631839</td>
      <td>5.568084</td>
      <td>11.044143</td>
      <td>10.992162</td>
      <td>5.398625</td>
      <td>...</td>
      <td>8.322062</td>
      <td>7.450691</td>
      <td>8.658371</td>
      <td>5.495414</td>
      <td>8.656046</td>
      <td>6.389019</td>
      <td>6.657073</td>
      <td>8.043682</td>
      <td>10.209835</td>
      <td>LumA</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 48 columns</p>
</div>




```python
gene_exp_T['brca_subtype'].value_counts()
```




    LumA           679
    LumB           461
    Her2           220
    Basal          199
    claudin-low    199
    Normal-like    140
    NC               6
    Name: brca_subtype, dtype: int64



### Running PCA

#### Without Claudin-low and NC


```python
# Get Features by taking transpose
features = gene_exp_T
features = features[features['brca_subtype'].isin(['claudin-low', 'NC']) == False]
sampleId_to_brca_subtype_map = dict(zip(features.index.tolist(), features['brca_subtype']))
features = features.drop(columns=['brca_subtype'])

# Colors
color_map = {'Basal': 'red',
             'Her2': 'green',
             'LumA': 'blue',
             'LumB': 'orange',
             'Normal-like' : 'black'}

# Apply PCA
pca = PCA(n_components = 2)
pca.fit(features)
df_pca = pd.DataFrame(pca.transform(features), columns=['PC1', 'PC2'], index=features.index)
plt.scatter(df_pca['PC1'], df_pca['PC2'],
            color=[color_map[sampleId_to_brca_subtype_map[i]] for i in df_pca.index.tolist()])

# Add the axis labels
plt.xlabel('PC 1 (%.2f%%)' % (pca.explained_variance_ratio_[0]*100))
plt.ylabel('PC 2 (%.2f%%)' % (pca.explained_variance_ratio_[1]*100)) 

handlelist = [plt.plot([], marker="o", ls="", color=color)[0] for color in color_map.values()]
plt.legend(handlelist,color_map.keys(),bbox_to_anchor=(1,1), loc="upper left")
plt.savefig('./Plots/PCAwithoutClaudinLow.png')

plt.show()
```


![png](images/output_43_0.png)


#### With Claudin-Low and NC


```python
# Get Features by taking transpose
features = gene_exp_T
sampleId_to_brca_subtype_map = dict(zip(features.index.tolist(), features['brca_subtype']))
features = features.drop(columns=['brca_subtype'])

# Colors
color_map = {'Basal': 'red',
             'claudin-low' : 'yellow',
             'NC' : 'pink',
             'Her2': 'green',
             'LumA': 'blue',
             'LumB': 'orange',
             'Normal-like' : 'black'}

# Apply PCA
pca = PCA(n_components = 2)
pca.fit(features)
df_pca = pd.DataFrame(pca.transform(features), columns=['PC1', 'PC2'], index=features.index)
plt.scatter(df_pca['PC1'], df_pca['PC2'],
            color=[color_map[sampleId_to_brca_subtype_map[i]] for i in df_pca.index.tolist()])

# Add the axis labels
plt.xlabel('PC 1 (%.2f%%)' % (pca.explained_variance_ratio_[0]*100))
plt.ylabel('PC 2 (%.2f%%)' % (pca.explained_variance_ratio_[1]*100)) 

handlelist = [plt.plot([], marker="o", ls="", color=color)[0] for color in color_map.values()]
plt.legend(handlelist,color_map.keys(),bbox_to_anchor=(1,1), loc="upper left")
plt.savefig('./Plots/PCAwithClaudinLow.png')

plt.show()
```


![png](images/output_45_0.png)


### Analysis on Phantusus
#### Sort the gene_exp_T using brca_subtype then remove the column before taking transpose
We want genes in rows and samples in columns


```python
gct_df = gene_exp_T.sort_values(by ='brca_subtype').drop(columns=['brca_subtype']).T
```
#### Make a GCToo object and save it as gct file which can be inputted on phantusus directly


```python
# Need to make a GCToo object to save it as a gct file
from cmapPy.pandasGEXpress.GCToo import GCToo
from cmapPy.pandasGEXpress.write_gct import write
```

```python
metadata = gene_exp_T[['brca_subtype']]
gct_Too_obj = GCToo(gct_df, col_metadata_df= metadata)
write(gct_Too_obj, './Generated Files/gene_exp_metabrics.gct')
```
GCT Analysis Link : http://52.9.27.18:8000/?session=x003fdad706367a
